soccerModule.directive('ssGoalieChart', function(StatsService) {
    return {
        scope: {
            stats: "=stats"
        },
        link: function (scope, element, attrs) {
            scope.$on('statsSelected', buildChart);

            function buildChart() {
                var data = {
                    labels: [],
                    datasets: [{
                        label: "В среднем пропущено в качестве голкипера",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: []
                    }]
                };
                for (var i in scope.stats.playerStats) {
                    var player = scope.stats.playerStats[i];

                    data.labels.push(StatsService.playerById(player.playerId, scope.stats.allPlayers).name);
                    data.datasets[0].data.push(player.double.goalie.goalsConcededPerGame);
                }

                var ctx = element.get(0).getContext("2d");
                var chart = new Chart(ctx).Bar(data);
            }
        }
    };
});