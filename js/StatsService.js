soccerModule.service('StatsService', function () {
    return {
        playerById: function (id, allPlayers) {
            for (var i = 0; i < allPlayers.length; i++) {
                if (allPlayers[i]._id == id) {
                    return allPlayers[i];
                }
            }
            return null;
        }
    };
});