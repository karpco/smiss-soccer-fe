soccerModule.directive('ssMatchDetails', function(StatsService, $http, $compile) {
    return {
        scope: {
            matchId: '=ssMatchDetails'
        },
        link: function (scope, element, attrs) {
            var matchInfo = {
                id: scope.matchId,
                shown: false,
                details: null
            };
            var info = $('<tr style="display: none"><td colspan="4"><ul class="match-details"><li ng-repeat="game in matchInfo.details.games">' +
                    '{{game.t1}} : {{game.t2}}' +
                '</li></ul>' +
                '<span class="delete-game-span"><button type="button" class="btn btn-danger" ng-click="deleteMatch(matchInfo.details._id)" ' +
                'ng-disabled="deletionInProgress">Удалить</button>' +
                '</span></td></tr>');
            element.parent().parent().after(info);

            element.click(function () {
                if (!matchInfo.details) {
                    $http.get('/api/match/' + matchInfo.id).then(function (response) {
                        matchInfo.details = response.data;

                        matchInfo.details.games = matchInfo.details.games.filter(
                            function(game) { return game.t1 != 0 || game.t2 != 0; });

                        //console.log(matchInfo.details);
                        var infoScope = scope.$new();
                        infoScope.matchInfo = matchInfo;
                        infoScope.deletionInProgress = false;
                        infoScope.deleteMatch = deleteMatchFn(infoScope);
                        $compile(info)(infoScope);

                        info.show();
                        matchInfo.shown = !matchInfo.shown;
                    });
                } else {
                    if (matchInfo.shown) {
                        info.hide();
                    } else {
                        info.show();
                    }
                    matchInfo.shown = !matchInfo.shown;
                }
            });

            function deleteMatchFn(scope) {
                return function (gameId) {
                    scope.deletionInProgress = true;
                    $http.delete('/api/match/' + gameId).then(function (response) {
                        scope.$emit("reload-stats");
                        scope.deletionInProgress = false;
                    });
                }
            }
        }
    };
});
