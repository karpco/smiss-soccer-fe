soccerModule.controller('MatchController', function ($scope, $http) {
    $scope.allPlayers = [];
    $http.get('/api/player', $scope.match).then(function (response) {
        $scope.allPlayers = response.data.filter(function(player){return !player.disabled;});
    });

    $scope.allPositions = [
        'атака', 'защита', ''
    ];

    function initMatch(match) {
        match.date = new Date();
        match.teamSize = 2;
        match.teams = [{
            'id': 't1',
            'name': 'Окно',
            'teammates': []
        }, {
            'id': 't2',
            'name': 'Стол',
            'teammates': []
        }];
        match.games = [];
    }

    $scope.match = {};
    initMatch($scope.match);

    $scope.$watch('match.teamSize', function () {
        $scope.maxGameCount = {1: 3, 2: 5}[$scope.match.teamSize];

        // initialize default teams
        for (var t in $scope.match.teams) {
            $scope.match.teams[t].teammates = [];
            for (var s = 0; s < $scope.match.teamSize; s++) {
                $scope.match.teams[t].teammates.push({
                    'position': $scope.match.teamSize == 2 ? $scope.allPositions[s] : $scope.allPositions[2]
                });
            }
        }

        // initialize games
        $scope.match.games = [];
        for (var g = 0; g < $scope.maxGameCount; g++) {
            $scope.match.games.push({
                't1': 0, 't2': 0,
                'isCompleted': function () {
                    return this.t1 == 5 || this.t2 == 5;
                }
            });
        }
    });

    $scope.savingInProgress = false;
    $scope.saveGame = function () {
        //console.log(JSON.stringify($scope.match));
        $scope.alerts = [];
        $scope.savingInProgress = true;

        $http.put('/api/match', $scope.match).then(function (response) {
            var alert = {
                type: "success",
                message: "Матч сохранен"
            };
            $scope.alerts.push(alert);
            $scope.savingInProgress = false;
            initMatch($scope.match);
        }, function (response) {
            var alert = {
                type: "danger",
                message: "Ошибка при сохранении: " + response.status
            };
            $scope.alerts.push(alert);
            $scope.savingInProgress = false;
        });
    };

    $scope.playerById = function (id) {
        for (var i = 0; i < $scope.allPlayers.length; i++) {
            if ($scope.allPlayers[i]._id == id) {
                return $scope.allPlayers[i];
            }
        }
        return null;
    };

    $scope.alerts = [];
    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.openDatepicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
});
