soccerModule.controller('StatsController', function ($scope, $http, StatsService) {
    $scope.stats = {
        matches: {
            allMatches: [],
            page: 1,
            frame: [],
            pageSize: 10
        },
        allPlayers: [],
        playerStats: [],
        teamStats: [],
        season: {
            playerStats: []
        }
    };

    $scope.sides = ['Окно', 'Стол'];

    $scope.playerById = function (id) {
        return StatsService.playerById(id, $scope.stats.allPlayers);
    };

    $scope.onlyEnabledPlayers = function (playerStats) {
        return !$scope.playerById(playerStats.playerId).disabled;
    };

    $scope.statsSelected = function() {
        $http.get('/api/match?count=-1').then(function (response) {
            $scope.stats.matches.allMatches = response.data;
            $scope.stats.matches.allMatches.forEach(function (match) {
                match.teams["Окно"] = teamByName(match.teams, "Окно");
                match.teams["Стол"] = teamByName(match.teams, "Стол");

                match.score = matchScore(match);
            });
            $scope.goToMatchesPage();

            //console.log($scope.stats.matches.allMatches);
        }, function (response) {
            var alert = {
                type: "danger",
                message: "Ошибка при получении данных: " + response.status
            };
            $scope.alerts.push(alert);
        });

        $http.get('/api/player').then(function (response) {
            $scope.stats.allPlayers = response.data;
        });

        $http.get('/api/stats/playerStats').then(function (response) {
            $scope.stats.playerStats = response.data;
            $scope.stats.playerStats.forEach(function (ps) {
                ps.double.winPercent = +(ps.double.totalWinGameCount/ps.double.totalGameCount * 100).toFixed(0)
            });
            $scope.stats.playerStats.sort(function (p1, p2) {
                var s2 = p2.double.simpleScore || -10;
                var s1 = p1.double.simpleScore || -10;

                return s2 - s1;
            });
        });

        $http.get('/api/stats/playerStats?season=true').then(function (response) {
            $scope.stats.season.playerStats = response.data;
            $scope.stats.season.playerStats.forEach(function (ps) {
                ps.double.winPercent = +(ps.double.totalWinGameCount/ps.double.totalGameCount * 100).toFixed(0)
            });
            $scope.stats.season.playerStats.sort(function (p1, p2) {
                var s2 = p2.double.simpleScore || -10;
                var s1 = p1.double.simpleScore || -10;

                return s2 - s1;
            });
        });

        $http.get('/api/stats/teamStats').then(function (response) {
            $scope.stats.teamStats = response.data.teamStats;
            $scope.stats.sideStats = response.data.sideStats;

            // JavaScript allows to write incredibly complicated code for relatively simple things
            // just counting total values here for single and double (summing sides)
            $scope.stats.sideStats.total = ['single', 'double'].reduce((total, teamSize) => {
                total[teamSize] = $scope.sides.reduce((sum, side) => {
                    var sideStats = $scope.stats.sideStats[teamSize];
                    return sum += sideStats[side]
                }, 0);
                return total;
            }, {});

        });

        //$scope.$broadcast("statsSelected");
    };

    $scope.$on('reload-stats', function () {
        $scope.statsSelected();
    });

    $scope.showWithSign = function (number) {
        return (number > 0 ? "+" : "") + number;
    };

    $scope.goToMatchesPage = function () {
        $scope.stats.matches.frame = $scope.stats.matches.allMatches.slice($scope.stats.matches.pageSize * ($scope.stats.matches.page - 1),
            $scope.stats.matches.pageSize * $scope.stats.matches.page);
    };

    $scope.currentMonth = moment().locale("ru").format("MMMM YYYY");

    function teamByName(teams, name) {
        for (var i = 0; i < teams.length; i++) {
            if (teams[i].name == name) {
                return teams[i];
            }
        }
    }

    function matchScore(match) {
        var wins = {
            "t1": 0,
            "t2": 0
        };
        for (var i = 0; i < match.games.length; i++) {
            if (match.games[i].t1 > match.games[i].t2) {
                wins.t1++;
            } else if (match.games[i].t1 < match.games[i].t2) {
                wins.t2++;
            } else {
                // incorrectly saved, ignoring
            }
        }
        return wins;
    }

    $scope.progressCssClass = function (progress, direction) {
        var cssClass;
        progress *= direction;
        if (progress > 0) {
            cssClass = "progress-positive";
        } else if (progress < 0) {
            cssClass = "progress-negative";
        } else {
            cssClass = "progress-neutral";
        }
        return cssClass;
    };
});
